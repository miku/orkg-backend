package eu.tib.orkg.prototype.statements.domain.model

data class Literal(
    val id: LiteralId?,
    val label: String
)
