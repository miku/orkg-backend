= ORKG Backend Prototype
Manuel Prinz <manuel.prinz@tib.eu>

:icons: font
:apidoc_url: http://orkg.git.tib.eu/orkg-prototype/api-doc/

This repository contains a backend prototype for the ORKG based on the https://spring.io/[Spring Framework].
It is written in https://kotlinlang.org/[Kotlin] as a proof-of-concept and for experimenting with a possible architecture and technologies.

== Building and running

=== Prerequisites

The prototype can be run stand-alone.
However, it needs other services in order to start properly.
Services are managed via https://www.docker.com/community-edition[Docker (CE)] and https://docs.docker.com/compose/[Docker Compose].
Please follow the respective installation instructions for your operating system or distribution.

=== Stand-alone application

NOTE: Running the application stand-alone is mostly for development purposes.
      It still needs Neo4j configured and running.
      If you want to test the application, please refer to the section <<Building a Docker image>> below.
      Alternatively, build the necessary images yourself by following the advice in the section <<Docker images>> (not recommended).

To build and run the prototype, type:

    ./gradlew bootRun

This will start a server running on http://localhost:8000.

The REST entry-point is http://localhost:8000/api/.

=== Building a Docker image

NOTE: The following commands assume that you added your user to the `docker` group https://docs.docker.com/install/linux/linux-postinstall/[as suggested in the documentation].
      If you did not do so, you have to prefix all commands with `sudo`.

The application can also run as a Docker container.
To build the image(s), run:

    ./gradlew docker

This will build the application and create the Docker image.
The application can be run via https://docs.docker.com/compose/[Docker Compose].
After installing it, run:

    docker-compose up -d

This will start the application image and all dependent containers.
It will take care of linking all containers so the services can see each other.
You can start and connect all services yourself but this is not recommended.

The application can be accessed via http://localhost:8000/.
The other services can be accessed via the URLs described in the table "<<endpoints>>".

To diagnose problems, check the logs with:

    docker-compose logs -f

WARNING: The following steps will destroy all data you saved to the database!

To restart from scratch, run:

    docker-compose stop
    docker-compose rm

== Docker images

NOTE: This section is only relevant if you want to work with some of the components in isolation.
      Please refer to the section <<Building a Docker image>> if you want to run the application via Docker.

=== Building the images

To build all Docker images (that contain the service dependencies), run:

    ./gradlew docker

If you want to build only specific images, either

[loweralpha]
. call `./gradlew :docker/IMAGE:docker` from the project root directory, or
. go to a `docker/IMAGE` directory and call `../../gradlew docker` there,

where `IMAGE` is the name of a sub-directory under `docker/`.

=== Running the images

The easiest way to start Docker containers from the images is via https://docs.docker.com/compose/[Docker Compose].
After installing it, run:

    docker-compose -f docker/IMAGE/docker-compose.yml up -d

where `IMAGE` is the name of a sub-directory under `docker/`.

=== Image end-points

The images will provide the following end-points:

.Images and their default end-points
[[endpoints]]
[cols=3*,options=header]
|===
|Image
|Container name
|End-points

|`orkg/prototype`
|`orkgprototype_prototype_1`
|http://localhost:8000/api/ (REST API)

|`orkg/neo4j-dev`
|`orkgprototype_neo4j_1`
| http://localhost:7474/browser/ +
bolt://localhost:7687

|===

=== Notes on the Neo4j Docker image

The Neo4j Docker image includes the following extensions:

* https://github.com/neo4j-contrib/neo4j-apoc-procedures[Awesome Procedures On Cypher (APOC)]
* https://github.com/jbarrasa/neosemantics[Neo4j Semantics Extension]

They are not used be the ORKG (as of now) but are very convenient during development.
There is no need to install them when deploying the ORKG.
Please check their specific documentation if you want to make use of them.

=== REST API documentation

The documentation can be found at {apidoc_url}.

NOTE: The documentation is currently only accessible from within the TIB network (i.e., also not accessible via Eduroam).
